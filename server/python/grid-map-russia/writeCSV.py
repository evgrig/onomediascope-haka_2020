from sshtunnel import SSHTunnelForwarder
import pandas as pd
import psycopg2 as db

# ssh variables
host = '95.216.208.74'
localhost = '127.0.0.1'
ssh_username = 'ono'
ssh_pass = 'It7RvwD7'

# database variables
user='server'
password='server'
database='ONONewsDB'

def query(q):
     with SSHTunnelForwarder(
          (host, 22),
          ssh_username=ssh_username,
          ssh_password=ssh_pass,
          remote_bind_address=(localhost, 5432)
     ) as server:
          conn = db.connect(host=localhost,
          port=server.local_bind_port,
          user=user,
          password=password,
          database=database)
          return pd.read_sql_query(q, conn)

def writeCSV():
     df = query('''select region_id, theme, count(theme) from posts_vk
                    JOIN sources ON posts_vk.source_id = sources.id
                    WHERE theme IS NOT null
                    GROUP BY theme, region_id
                    ORDER BY region_id''')

     df_res = pd.read_csv('data/OnoExample.csv')

     regions = [72193, 72194, 77669, 77677, 79374, 140337, 234788622, 235500067]
     themes = ['город', 'силовики', 'коррупция', 'мошенничество']

     for region_id in regions:
          for theme in themes:
               try:
                    df_res.loc[df_res.region_id == region_id, theme] = df.loc[(df.region_id == region_id) & (df.theme == theme), 'count'].values[0]
               except Exception as e:
                    continue
     
     df_res.to_csv('data/OnoTable.csv')

writeCSV()