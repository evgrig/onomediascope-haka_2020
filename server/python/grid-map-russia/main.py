from plot_grid_map import plot_grid_map
# from writeCSV import writeCSV
import pandas as pd


df = pd.read_csv('data/OnoTable.csv')
features_list = ['город',
                 'силовики',
                 'коррупция',
                 'мошенничество',
                 'Ничего']

# writeCSV()

plot_grid_map(df, features_list,
              map_title='ONOMediaScope',
              shuffle=False,
              pixels=20,
              bar_plot=True,
              tile_names='subj_rus',
              is_pallete_custom=True,
              pallete_custom=['#B03A2E',
                              '#D35400',
                              '#5499C7',
                              '#138D75',
                              '#FFFFFF'],
              output_filname='test.html',
              fed_distr_color=True)
