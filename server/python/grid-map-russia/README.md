## [Live preview](https://spbroma.github.io/grid_map/plain)

[Подробное описание](/../wikis/home)

Скрипт позволяет генерировать плиточную карту России, заполняя её данными из подготовленного датасета.  
За основу датасета следует взять файл `grid_map_rus - blank.csv` либо `grid_map_rus.xlsx`.  
Для примера используется датасет с валовым региональным продуктом в файле `grid_map_rus - example.csv`  

Алгоритм заполнения плиток реализован в функции `tile_generator` и может быть модифицирован в зависимости от нужд.


``` python
def plot_grid_map(df, features_list,
                  map_title='Плиточная карта',
                  tile_names="subj_rus",
                  map_width=1000,
                  map_height=700,
                  bar_plot=False,
                  shuffle=True,
                  pixels=30,
                  is_pallete_custom=False,
                  pallete_name='Spectral',
                  pallete_custom=['#aaaacc', '#666688'],
                  output_filname='index.html',
                  fed_distr_color = True):
```


