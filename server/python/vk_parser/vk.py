'''
Requirements:
pip install vk-api
'''

import vk_api
from ONOsum import get_headline, get_headline_
from sqlalchemy import create_engine
from datetime import datetime
from nltk.stem.snowball import SnowballStemmer
from random import randint
import pandas as pd
import numpy as np
import json
import os
import psycopg2
import re
import sys
import time

'''
https://vk.com/public56957305
https://vk.com/public121071234
https://vk.com/public10889156
https://vk.com/public33041211
https://vk.com/public108370037
https://vk.com/public117312010
https://vk.com/public23769931
https://vk.com/public108164886
https://vk.com/public98018205
https://vk.com/public59805572
'''

vk_login = '79268153046'
vk_password = 'iO2k3JcC6QGQPSb'
vk_token = None
url_wall = 'https://vk.com/wall'
# group_id = int(sys.argv[-1])
# group_id = -56957305

dir = os.path.dirname(__file__)

# Main config
current = os.path.join(dir, '../../config/config.json')
with open(current, encoding='utf8') as json_config:
    config = json.load(json_config)
    connection_string_main = config['database']['connectionStringMain']
    vk_expected_words = config['vkExpectedWords']
    vk_stop_words = config['vkStopWords']
    # connection_string_main = 'postgres://postgres:BeacBd7t7b$r68d@localhost:5432/euspb-2019-onobot'
    # vk_expected_words = ["авария", "арест", "асфальт", "бастовать", "бездействие", "бездействовать", "безопасно", "больница", "бригада", "водопровод", "выжил", "вызов", "выявить", "госпитализация", "госпитализировать", "данные", "диагноз", "договор", "дольщик", "драка", "животных", "забастовка", "заброшенный", "задержан", "задержать", "задержка", "закрыть", "зарегистрирован", "затапливает", "избили", "издевательства", "инспекция", "исключить", "контроль", "малолетний", "массовая", "микрорайон", "мошенничество", "мусор", "наброситься", "насилие", "невозможно", "нехватка", "обыск", "ограбление", "осторожно", "отказ", "отопление", "отрава", "отстоять", "пациент", "переполнен", "пикет", "плакат", "погиб", "погорельцы", "пожар", "покончить", "полигон", "поликлиника", "полицейский", "полиция", "помощь", "поставщик", "приговор", "проверка", "пролежни", "прорыв", "протест", "профилактика", "раскопано", "расселение", "ребенок", "руководитель", "самоубийство", "свалка", "скончался", "скорая", "скрыть", "студент", "травма", "травмпункт", "требования", "убитый", "уволен", "уволить", "уволить", "увольнение", "украли", "умер", "уничтожить", "ученик", "центр города", "чиновник"]
    # vk_stop_words = ["розыгрыш", "конкурс"]

# Vk config
vk_config = os.path.join(dir, 'vk_config.v2.json')
with open(vk_config, encoding='utf8') as json_config:
    config = json.load(json_config)
    try:
        vk_token = config[vk_login]['token']['app6222115']['scope_140492255']['access_token']
    except Exception as e:
        vk_token = None


# Авторизация
def auth(login, password):
    session = vk_api.VkApi(token=vk_token)
    try:
        session.auth()
    except Exception as e:
        session = vk_api.VkApi(login=login, password=password, auth_handler=auth_handler)
        session.auth()
    vk = session.get_api()
    return vk


# Двухэтапная аутентификация
def auth_handler():
    code = input("Confirmation code\n> ")
    remember_device = True
    return code, remember_device


# Список источников из базы
def get_sources():
    engine = create_engine(connection_string_main)
    sources_query = '''SELECT id, url FROM sources WHERE format = %(method)s AND id > 567'''
    sources = pd.read_sql(sources_query, engine, params={'method': 'vk'})
    sources['vk_id'] = pd.Series([int(re.sub('\D', '', source)) * -1 for source in sources.url.values.tolist()])
    return sources


# Извлечение постов
def extraction_posts(posts, group, source_id, source_vk_id):
    columns = ['source_id', 'source_vk_id', 'source_title', 'article_url', 'article_title', 'article_text', 'published',
               'comments_count', 'likes_count', 'reposts_count', 'views_count', 'attachment', 'rating']
    df_posts = pd.DataFrame(columns=columns)

    for post in posts.get('items'):
        if post.get('post_type') == 'post':
            try: 
                df_posts = df_posts.append({'source_id': source_id,
                                            'source_vk_id': source_vk_id,
                                            'source_title': group[0].get('name'),
                                            'article_url': f'{url_wall}{group[0].get("id")}_{post.get("id")}',
                                            'article_title': None,
                                            'article_text': post.get('text'),
                                            'published': datetime.fromtimestamp(post.get('date')),
                                            'comments_count': post.get('comments').get('count'),
                                            'likes_count': post.get('likes').get('count'),
                                            'reposts_count': post.get('reposts').get('count'),
                                            'views_count': post.get('views').get('count'),
                                            'attachment': [attachment.get(attachment.get('type')).get('sizes')[-1].get(
                                                'url')
                                                              for attachment in post.get('attachments', [])
                                                              if attachment.get('type') == 'photo'] or None,
                                            'rating': None
                                            }, ignore_index=True)
            except Exception as e:
                print(e)
    return df_posts


# Удаление существующих постов
def check_duplicates(posts, id):
    engine = create_engine(connection_string_main)
    source_posts_query = '''SELECT article_url FROM posts_vk WHERE source_id = %(src)s'''
    source_posts = pd.read_sql(source_posts_query, engine, params={'src': int(id)})
    return posts[~posts.article_url.isin(source_posts.article_url.values.tolist())]


# Фильтрация "мусора" (по размеру)
def filter_posts_by_len(posts):
    posts = posts[posts.article_text.str.len() > 250]
    posts = posts.reset_index().drop(columns=['index'])
    return posts


# Фильтрация "мусора" (по содержанию)
def filter_posts(posts):
    emojies = re.compile("["
                         u"\U0001F600-\U0001F64F"  # emoticons
                         u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                         u"\U0001F680-\U0001F6FF"  # transport & map symbols
                         u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                         u"\U00002500-\U00002BEF"  # chinese char
                         u"\U00002702-\U000027B0"
                         u"\U00002702-\U000027B0"
                         u"\U000024C2-\U0001F251"
                         u"\U0001f926-\U0001f937"
                         u"\U00010000-\U0010ffff"
                         u"\u2640-\u2642"
                         u"\u2600-\u2B55"
                         u"\u200d"
                         u"\u23cf"
                         u"\u23e9"
                         u"\u231a"
                         u"\ufe0f"  # dingbats
                         u"\u3030"
                         "]+", re.UNICODE)

    posts.article_text = pd.Series([emojies.sub(r'', post) for post in posts.article_text])
    posts.article_text = pd.Series([post.replace(u'\n', ' ') for post in posts.article_text])
    posts.article_text = pd.Series([re.sub(' +', ' ', post) for post in posts.article_text])
    posts.article_text = pd.Series([post.strip() for post in posts.article_text])
    posts.article_title = pd.Series([get_headline(post)[:255] for post in posts.article_text])
    try:
        posts.article_title = pd.Series([title[:-1] if title[-1] == '.' else title for title in posts.article_title])
    except Exception as e:
        print('< Error! >')
        print(e)

    # posts = posts[posts.article_text.str.len() > 250]
    # posts = posts.reset_index().drop(columns=['index'])

    return posts


# Рейтинг
def rating_by_words(posts):
    stemmer = SnowballStemmer("russian")
    vk_expected_words_stem = [stemmer.stem(word) for word in vk_expected_words]
    vk_stop_words_stem = [stemmer.stem(word) for word in vk_stop_words]

    for idx, article in posts.iterrows():
        # positive = sum([word in article.article_text for word in vk_expected_words_stem]) / len(article.article_text.split())
        positive = len(re.findall('|'.join(vk_expected_words_stem), article.article_text)) / len(
            article.article_text.split())
        negative = len(re.findall('|'.join(vk_stop_words_stem), article.article_text)) / len(
            article.article_text.split())
        negative_ht = len(re.findall(r'\B#\w*[a-zA-Zа-яА-Я0-9]+\w*', article.article_text)) / len(
            article.article_text.split())

        posts.loc[idx, 'rating'] = positive - negative - (negative_ht * 1.5)

    return posts


# Запись в БД
def write_to_db_posts(posts):
    conn = psycopg2.connect(connection_string_main)
    db = conn.cursor()
    query = '''INSERT INTO posts_vk (source_id, source_vk_id, source_title, article_url, article_title, article_text, published,
                                     comments_count, likes_count, reposts_count, views_count, attachment, rating)        
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING;'''

    for idx, post in posts.iterrows():
        try:
            db.execute(query, tuple(post.values))
        except Exception as e:
            print(e)
            continue
    conn.commit()
    db.close()


# main

t = time.time()
print(datetime.fromtimestamp(t).time())

mins = randint(1, 20)
print('Sleeping for', mins, 'min')
print('...')
# time.sleep(mins * 60)

vk = auth(vk_login, vk_password)
print('Vk login successful in', np.round(time.time() - t, 2), 's')
t = time.time()

sources_list = get_sources()
print('Sources received in', np.round(time.time() - t, 2), 's')
t = time.time()

for idx, source in sources_list.iterrows():

    vk_posts = vk.wall.get(owner_id=source.vk_id, count=50)
    vk_group = vk.groups.getById(group_id=abs(source.vk_id))
    print('Group posts & meta received in', np.round(time.time() - t, 2), 's')
    t = time.time()

    df_posts = extraction_posts(vk_posts, vk_group, source.id, source.vk_id)
    print('Posts extracted in', np.round(time.time() - t, 2), 's')
    t = time.time()

    df_posts = check_duplicates(df_posts, source.id)
    print('Duplicates removed in', np.round(time.time() - t, 2), 's')
    t = time.time()

    df_posts = filter_posts_by_len(df_posts)
    print('Posts filtered by len in', np.round(time.time() - t, 2), 's')
    t = time.time()

    if len(df_posts):
        df_posts = filter_posts(df_posts)
        print('Posts filtered in', np.round(time.time() - t, 2), 's')
        t = time.time()

        df_posts = rating_by_words(df_posts)
        print('Ratings counted in', np.round(time.time() - t, 2), 's')
        t = time.time()

        write_to_db_posts(df_posts)
        print('Posts written', np.round(time.time() - t, 2), 's')
    else:
        print('No news posts')
