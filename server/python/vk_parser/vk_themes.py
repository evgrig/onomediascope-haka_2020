'''
SUMMARY OF THE SCRIPT:
    1) it takes data from table %TABLE you pass last in parameters
    2) it needs a table %TABLE from archiveDB to learn on
    3) it constructs the model and then applies it to the current data
    4) it affects the columns "theme", "theme_score", "theme_list" in %TABLE
'''
import os
import sys
import pandas as pd
import json
import time
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords
import operator
import guidedlda


import psycopg2
from sqlalchemy import create_engine

connection_string_main = ""
connection_string_archive = ""

dir_ = os.path.dirname(__file__)
file_path = os.path.join(dir_, '../../config/config.json')
#file_path = '../TheOnoBot-spb_dev/news_collecting/config/config.json'

with open(file_path, encoding="utf8") as json_file:
    config = json.load(json_file)
    connection_string_main = config["database"]["connectionStringMain"]
    connection_string_archive = config["database"]["connectionStringMain"]
    stop_words = config["themesStopWords"]
    tmp_thema = config['haka_themes']

thema = {}
seed_topic_list = []
for cnt in range(len(tmp_thema)):
    thema[cnt] = tmp_thema[str(cnt)]['name']
    seed_topic_list.append(tmp_thema[str(cnt)]['words'])


engine = create_engine(connection_string_main)
# checking tablename
tablename = 'posts_vk' # sys.argv[-1]
try:
    pd.read_sql("SELECT id, article_title, full_text_preproc FROM {};".format(tablename), engine)
except Exception as e:
    print('Failed to load table {}; resetting to default "articles"'.format(tablename))
    tablename = 'posts_vk'
    print(e)

def load_from_db():
    df = pd.read_sql("SELECT id, article_title, full_text_preproc FROM {} WHERE full_text_preproc IS NOT NULL AND rating > 0;".format(tablename), engine)
    return df

def thema_list(doc_topic, x):
    """
    Вспомогательная функция, возращающая список тем для данного текста с вероятностью более 25%

    parameters:
    ----------
    doc_topic - numpy array - матрица вероятности принадлежности текста к определенной теме
    x - int - index данного текста

    returns:
    ----------
    thems - list, список тем для данного текста с вероятностью более 25%

    """
    thems=[]
    a = np.where(doc_topic[x] > 0.25)[0]
    b = np.argsort(np.array(doc_topic[x]))
    result = [x for x in a if x in b]
    for ind in result:
        thems.append(thema[ind])
    return thems

def news_clustering_fit(data, column_name):
    """
    функция, обучающая semi-supervised lda модель

    parameters:
    ----------
    data - pandas dataframe, содержащий набор текста для обучения
    column_name - string, название колонки датафрейса data, содержащей отчищенный текст

    returns:
    ----------
    vectorizer - обученная модель, переводящая текст в матричный формат
    model - обученная модель - semi-supervised lda
    """
    data = data.drop_duplicates().dropna()

    STOP_WORDS = set(stopwords.words('russian')) | set(stopwords.words('english'))
    stop_words.extend(STOP_WORDS)

    vectorizer = CountVectorizer(stop_words = stop_words,max_df=0.999,min_df=0.001,ngram_range=(1,2))
    X_train = vectorizer.fit_transform(data[column_name])

    vocab = vectorizer.vocabulary_
    vocab = sorted(list(vocab.items()), key=operator.itemgetter(1))
    vocab = [x[0] for x in vocab]
    word2id = dict((v, idx) for idx, v in enumerate(vocab))

    seed_topics = {}
    for t_id, st in enumerate(seed_topic_list):
         for word in st:
            try:
                seed_topics[word2id[word]] = t_id
            except: None

    model = guidedlda.GuidedLDA(n_topics=len(thema), n_iter=500, random_state=8, refresh=100)
    model.fit(X_train, seed_topics=seed_topics, seed_confidence=0.25)

    return vectorizer, model


def news_clustering_predict(data, column_name, vectorizer, model):
    """
    функция, предсказывающая тему новости

    parameters:
    ----------
    data - pandas dataframe, содержащий набор текста для предсказания
    column_name - string, название колонки датафрейса data, содержащей отчищенный текст
    vectorizer - обученная модель, переводящая текст в матричный формат
    model - обученная модель - semi-supervised lda

    returns:
    ----------
    data - pandas dataframe, содержащий набор текста для предсказания с добавлением колонок:
        - thema - название темы (с максимальной вероятностью)
        - thema_score - вероятность, что эта статья относится к этой теме
        - thema_list - список тем с вероятностью больше 20%
        - thema_topic_score - вероятность, этой (topic) конкретной темы

    """
    if (vectorizer is None) or (model is None):
        print('Models not found.')
    else:
        X_test = vectorizer.transform(data[column_name])
        doc_topic = model.transform(X_test)

        data = data.reset_index(drop=True).reset_index()

        data['theme'] = data['index'].apply(lambda x: thema[np.argmax(doc_topic[x])])
        data['theme_score'] = data['index'].apply(lambda x: np.max(doc_topic[x]))
        # data['themes_list'] = data['index'].apply(lambda x: thema_list(doc_topic,x))
    return data

def make_archive_model():
    engine_arch = create_engine(connection_string_archive)
    df_archive = pd.read_sql('SELECT id, full_text_preproc FROM {} ORDER BY published DESC LIMIT 10000;'.format('articles'), engine_arch)
    vectorizer, model = news_clustering_fit(df_archive, 'full_text_preproc')
    model_dict = {'vectorizer': vectorizer, 'model': model}
    return model_dict


def get_themes(df, model_dict):
    df_themes = news_clustering_predict(df, 'full_text_preproc',
                                        model_dict['vectorizer'],
                                        model_dict['model'])
    df = df.merge(df_themes)
    return df

def write_themes_to_db(df):
    conn = psycopg2.connect(connection_string_main)
    db = conn.cursor()
    queryArticles = """
                UPDATE {} SET theme = (%s), theme_score = (%s)
                WHERE id = (%s);
            """.format(tablename)
    for index, row in df.iterrows(): # better copy_from for large uploads
        id_, theme, theme_score = \
            row[['id', 'theme', 'theme_score']].values
        db.execute(queryArticles, (theme, theme_score, id_))
    conn.commit()
    db.close()
    conn.close()

t = time.time()
df = load_from_db()
print('Loaded DB in', np.round(time.time() - t, 2), 's'); t = time.time()
# themes
model_dict = make_archive_model()
print('Made themes model in', np.round(time.time() - t, 2), 's'); t = time.time()
df = get_themes(df, model_dict)
print('Calculated themes in', np.round(time.time() - t, 2), 's'); t = time.time()
write_themes_to_db(df)
print('Loaded to DB in', np.round(time.time() - t, 2), 's'); t = time.time()
