"""

"""

import json
import os
import pandas as pd
import psycopg2
import re
import sys

from datetime import datetime
from pymystem3 import Mystem

server = ""
path = os.path.dirname(__file__)
with open(os.path.join(path, '../../config/config.json'), encoding="utf8") as f:
    # Read config data from json.
    data = json.load(f)
    server = data["database"]["connectionStringMain"]
    f.close()


def load_from_db():
    df = pd.read_sql("""SELECT id, article_text, full_text_preproc
                          FROM posts_vk WHERE full_text_preproc IS NULL LIMIT 500""", server)
    return df


def mystem_normalize(text):
    mystem = Mystem()

    analysis = [el for el in mystem.analyze(text) if 'analysis' in el]
    tokens = [el['text'].lower() for el in analysis]
    lemmas = []
    pos_tags = []
    for el in analysis:
        if el['analysis']:
            lemmas.append(el['analysis'][0]['lex'])
            pos_tags.append(re.split('[,=]', el['analysis'][0]['gr'])[0])
        else:
            lemmas.append(el['text'].lower())
            pos_tags.append('UNK')
    tokens = [token for ind, token in enumerate(lemmas) if pos_tags[ind] in ['S', 'V', 'A']]

    return ' '.join(tokens)


"""MAIN"""

try:
    N = int(sys.argv[-2])
    STEP = int(sys.argv[-1])
except:
    N = None
    STEP = None

conn = psycopg2.connect(server)
db = conn.cursor()
query = """UPDATE posts_vk SET full_text_preproc=%s, updated_at=%s WHERE id = %s"""
df = load_from_db()
df = df.reset_index(drop=True)

set = []
if STEP:
    sum = len(df) - 1
    i = N
    while i < sum:
        set.append(i)
        i = i + STEP

if len(set):
    df = df.loc[set]

for index, row in df.iterrows():
    row['full_text_preproc'] = mystem_normalize(row['article_text'])

    db.execute(
        query,
        (row['full_text_preproc'], datetime.now().strftime("%Y-%m-%dT%H:%M:%S.000Z"), row["id"])
    )
    conn.commit()

db.close()
conn.close()
