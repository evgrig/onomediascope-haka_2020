import json
import pandas as pd
import numpy as np
import sys
import os



# 72193
# 72194
# 77669
# 79374
# 140337
# 234788622
# 235500067
# region_id = 72193
# region_id = int(sys.argv[-1])
res = {}
server = ""
path = os.path.dirname(__file__)
with open(os.path.join(path, '../../config/config.json'), encoding="utf8") as f:
    # Read config data from json.
    data = json.load(f)
    server = data["database"]["connectionStringMain"]
    tags = data["haka_wordcloud"]
    f.close()


def load_regions():
    df = pd.read_sql("""SELECT DISTINCT region_id FROM sources        
        WHERE format = '{}'""".format("vk"), server)
    return df.region_id

def load_from_db(region_id):
    df = pd.read_sql("""SELECT full_text_preproc FROM posts_vk
        JOIN sources ON posts_vk.source_id = sources.id
        WHERE sources.region_id = {}
        ORDER BY rating DESC""".format(region_id), server)
    return df


def word_count(str):
    words = str.split()
    words = [word for word in words if word in tags]
    for word in words:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1
    return counts

regions = load_regions()

for region in regions:

    counts = {}
    df = load_from_db(region)
    
    for idx, row in df.iterrows():
        try:
            word_count(row.full_text_preproc)
        except Exception as e:
            print(e)

    res[region] = []
    for key in counts:
        res[region].append({'value': key, 'count': counts[key]})

    print('')
    print(res[region])

# print(res)


with open(os.path.join(path, '../../config/wordcloud.json'), 'w') as outfile:
    json.dump(res, outfile)