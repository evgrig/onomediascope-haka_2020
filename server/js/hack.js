/* eslint-disable*/
const { sequelize } = require('../models');
const wc = require('../config');
//const JAC_SIM_THRESHOLD = config.get('jac_sim_threshold');
//
const trends = wc.get('haka_trends');

async function getNewsFromVk(req, res) {
  try {
    const region = req.params.region_id;
    const theme = req.params.theme || null;
    console.log('region', region, 'theme', theme)
    let query;
    
    if (theme !== 'null') {
      query = `SELECT source_title, article_url, rating, article_title, article_text, published, theme, sources.url, posts_vk.id
              FROM posts_vk
                  JOIN sources ON posts_vk.source_id = sources.id
                      WHERE sources.region_id = :region AND posts_vk.full_text_preproc LIKE '%${theme}%'
              ORDER BY rating DESC
              LIMIT 40`;
      const data = (await sequelize.query(
      query,
      { logging: console.log, replacements: { region }, type: sequelize.QueryTypes.SELECT },
      ));
      res.status(200).send(data);
      console.log('data', data)

    }
    else {
      query = `SELECT source_title, article_url, rating, article_title, article_text, published, theme, sources.url, posts_vk.id
              FROM posts_vk
                  JOIN sources ON posts_vk.source_id = sources.id
                      WHERE sources.region_id = :region
              ORDER BY rating DESC
              LIMIT 40`;
      const data = (await sequelize.query(
        query,
        { logging: console.log, replacements: { region }, type: sequelize.QueryTypes.SELECT },
        ));
      res.status(200).send(data);
      console.log('data', data)
    } 
          
     
  } catch (e) {
    console.log('e', e)
    res.status(500).send(e);
  }
}

async function getNewsTags(req, res) {

  const region = String(req.params.region_id);
  
  console.log(wc)
  // trands[region] 
  // console.log(trends)
  console.log(region)

  const data = [
    { value: 'JavaScript', count: 38 },
    { value: 'React', count: 30 },
    { value: 'Nodejs', count: 28 },
    { value: 'Express.js', count: 25 },
    { value: 'HTML5', count: 33 },
    { value: 'MongoDB', count: 18 },
    { value: 'CSS3', count: 20 },
  ];
  res.status(200).send(trends[region]);
}

module.exports = {
    getNewsFromVk,
    getNewsTags,
};
