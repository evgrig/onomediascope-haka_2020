const router = require('express').Router();
const hack = require('./hack');

router.get('/api/hack/vk/:region_id/:theme', hack.getNewsFromVk);
router.get('/api/hack/tags/:region_id', hack.getNewsTags);

module.exports = router;
