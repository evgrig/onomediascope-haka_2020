import React, { PureComponent } from 'react'
import { List, Icon, Spin } from 'antd'
import TweenOne from 'rc-tween-one'

import NewsListItem from './Components/NewsListItemComponent'

class NewsList extends PureComponent {
  state = {
    data: [
      {
        id: 1,
        animation: false,
        title: 'Малкин и Кучеров отличились в НХЛ',
        source: 'Ведомости, 15:34',
        logo:
          'https://yt3.ggpht.com/a/AGF-l7-_eF-6Q4kcbOI4rmWeuVA8dxlsYmj_FIyj2A=s900-c-k-c0xffffffff-no-rj-mo',
        sourceLink: '#',
        newsLink: '#',
        snipet:
          'По информации источника РИА Новости, уже сегодня вечером премьер может приехать в Белый дом на встречу с членами кабмина.',
      },
      {
        id: 2,
        animation: false,
        title: 'Фигуристы и биатлонисты из России солируют на юношеских ОИ',
        source: 'РИА Новости, 15:28',
        logo:
          'https://yt3.ggpht.com/a/AGF-l78xDlqbX9SyUKcId0wot83NK8HhtBwHJ6Az=s900-c-k-c0xffffffff-no-rj-mo',
        sourceLink: '#',
        newsLink: '#',
        snipet:
          'По информации источника РИА Новости, уже сегодня вечером премьер может приехать в Белый дом на встречу с членами кабмина.',
      },
      {
        id: 3,
        animation: false,
        title:
          'Палата представителей направила в Сенат США документы по импичменту Дональда Трампа',
        source: 'Дождь, 15:21',
        logo:
          'https://yt3.ggpht.com/a/AGF-l7_EJsAWgrYy5uhoUX360kx_yQkU4rHjgODZ=s900-c-k-c0xffffffff-no-rj-mo',
        sourceLink: '#',
        newsLink: '#',
        snipet:
          'По информации источника РИА Новости, уже сегодня вечером премьер может приехать в Белый дом на встречу с членами кабмина.',
      },
      {
        id: 4,
        animation: false,
        title:
          'Станислав Черчесов продолжит руководить сборной России по футболу',
        source: 'Медуза, 15:14',
        logo:
          'https://yt3.ggpht.com/a/AGF-l7-ouqZkEL_fo3WS6i0djXrE-ZkKkFP96jtERA=s900-c-k-c0xffffffff-no-rj-mo',
        sourceLink: '#',
        newsLink: '#',
        snipet:
          'По информации источника РИА Новости, уже сегодня вечером премьер может приехать в Белый дом на встречу с членами кабмина.',
      },
    ],
  }

  addNewItem = () => {
    const item = {
      id: Date.now(),
      animation: true,
      title:
        'Станислав Черчесов продолжит руководить сборной России по футболу',
      source: 'Медуза, 15:14',
      logo:
        'https://yt3.ggpht.com/a/AGF-l7-ouqZkEL_fo3WS6i0djXrE-ZkKkFP96jtERA=s900-c-k-c0xffffffff-no-rj-mo',
      sourceLink: '#',
      newsLink: '#',
      snipet:
        'По информации источника РИА Новости, уже сегодня вечером премьер может приехать в Белый дом на встречу с членами кабмина.',
    }
    this.setState({
      data: [item, ...this.state.data],
    })
    setTimeout(this.addNewItem, 3000)
  }

  componentDidMount = () => {
    if (this.props.animation) {
      setTimeout(this.addNewItem, 5000)
    }
  }

  render() {
    const data = this.props.data || this.state.data
    const isLoading = this.props.isLoading || false
    const antIcon = <Icon type="loading" style={{ fontSize: 40 }} spin />

    const items = data.map((item, index) =>
      item.animation ? (
        <TweenOne
          key={item.id}
          animation={[
            { backgroundColor: '#fffeee', duration: 0 },
            { backgroundColor: '#fff', duration: 4000 },
          ]}
        >
          <NewsListItem
            item={item}
            data={data}
            drillDown={this.props.drillDown}
            snipet={false}
          />
        </TweenOne>
      ) : (
        <NewsListItem
          item={item}
          data={data}
          drillDown={this.props.drillDown}
          snipet={false}
          key={item.id}
        />
      )
    )

    return (
      <>
        <List itemLayout="horizontal">{items}</List>
        {isLoading && (
          <div className="NewsList__loader">
            <Spin indicator={antIcon} />
          </div>
        )}
      </>
    )
  }
}

export default NewsList
