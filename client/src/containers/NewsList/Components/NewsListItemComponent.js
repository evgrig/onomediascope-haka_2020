import React, { PureComponent } from 'react'
import { List, Avatar, Popover, Icon } from 'antd'
import QueueAnim from 'rc-queue-anim'

import ClusterTitle from './ClusterTitleComponent'

class NewsListItem extends PureComponent {
  state = {
    isOpenTitles: false,
  }

  openTitles = () => {
    this.setState({
      isOpenTitles: true,
    })
  }

  closeTitles = () => {
    this.setState({
      isOpenTitles: false,
    })
  }

  render() {
    const { item, data } = this.props
    return (
      <List.Item>
        <List.Item.Meta
          avatar={
            this.props.drillDown ? (
              <Avatar className={'NewsList__avatar'}>
                {!this.state.isOpenTitles && (
                  <Icon
                    type="down-circle"
                    theme="filled"
                    className={'NewsList__avatar-icon'}
                    onClick={this.openTitles}
                  />
                )}

                {this.state.isOpenTitles && (
                  <Icon
                    type="up-circle"
                    theme="filled"
                    className={'NewsList__avatar-icon'}
                    onClick={this.closeTitles}
                  />
                )}
              </Avatar>
            ) : null
          }
          title={
            <Popover
              content={
                <div className={'NewsList__popover-snipet'}>{item.snipet}</div>
              }
            >
              <a href={item.newsLink} rel="noopener noreferrer" target="_blank">
                {item.title}
              </a>
            </Popover>
          }
          description={
            <>
              <a
                href={item.sourceLink}
                rel="noopener noreferrer"
                target="_blank"
                className={'NewsList__news-text'}
              >
                {item.source}
              </a>
              {this.props.snipet && <p>{item.snipet}</p>}
              <div className={'NewsList__titles-group'}>
                <QueueAnim
                  ease={['easeOutQuart', 'easeInOutQuart']}
                  type={['right', 'left']}
                >
                  {this.state.isOpenTitles
                    ? data.map((item, index) => (
                        <ClusterTitle key={index} item={item} />
                      ))
                    : null}
                </QueueAnim>
              </div>
            </>
          }
        />
      </List.Item>
    )
  }
}

export default NewsListItem
