import React, { PureComponent } from 'react'
import { Avatar, Comment, Popover } from 'antd'

class ClusterTitle extends PureComponent {
  render() {
    const { item } = this.props
    return (
      <Comment
        //actions={[<span key="comment-nested-reply-to">Reply to</span>]}
        className={'ClusterTitle'}
        author={
          <a href={item.sourceLink} rel="noopener noreferrer" target="_blank">
            {item.source}
          </a>
        }
        avatar={<Avatar src={item.logo} />}
        content={
          <p>
            <Popover
              content={
                <div className={'NewsList__popover-snipet'}>{item.snipet}</div>
              }
            >
              <a
                href={item.newsLink}
                rel="noopener noreferrer"
                target="_blank"
                className={'ClusterTitle__title'}
              >
                {item.title}
              </a>
            </Popover>
          </p>
        }
      ></Comment>
    )
  }
}

export default ClusterTitle
