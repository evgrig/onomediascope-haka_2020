import React, { PureComponent } from 'react'
import { Row, Col, Switch, Checkbox } from 'antd'
import { Helmet } from 'react-helmet'

import NewsList from '../../NewsList/NewsListComponent'

class Timeline extends PureComponent {
  state = {
    isOpenSources: true,
  }

  closeSources = () => {
    this.setState({
      isOpenSources: false,
    })
  }

  openSources = () => {
    this.setState({
      isOpenSources: true,
    })
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Лента</title>
        </Helmet>

        <h1>Лента</h1>

        <Row className={'SearchForm__container'}>
          <Col span={6}>
            <label className="Timeline__label">
              <Switch defaultChecked /> Cоцсети
            </label>
          </Col>
          <Col span={6}>
            <label className="Timeline__label">
              <Switch defaultChecked /> Телеграм-каналы
            </label>
          </Col>
          <Col span={6}>
            <label className="Timeline__label">
              <Switch defaultChecked /> Онлайн СМИ
            </label>
          </Col>
          <Col span={6}>
            <label className="Timeline__label">
              <Checkbox defaultChecked /> Показывать снипет
            </label>
          </Col>
        </Row>

        <NewsList />
      </div>
    )
  }
}

export default Timeline
