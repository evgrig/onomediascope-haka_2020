import React, { PureComponent } from 'react'
import { TagCloud } from 'react-tagcloud'
import { Icon, Spin, Popover } from 'antd'
import { connect } from 'react-redux'

import {
  getTags,
  setRegion,
  loadNews,
} from '../../../../redux/actions/TagsActions'

class TagsCloud extends PureComponent {
  getUrlVar() {
    let urlVar = window.location.search
    let arrayVar = []
    let valueAndKey = []
    let resultArray = []
    arrayVar = urlVar.substr(1).split('&')
    if (arrayVar[0] === '') return false
    for (let i = 0; i < arrayVar.length; i++) {
      valueAndKey = arrayVar[i].split('=')
      resultArray[valueAndKey[0]] = decodeURI(valueAndKey[1])
    }
    return resultArray
  }

  componentDidMount() {
    const urlVars = this.getUrlVar()
    this.props.getTags(urlVars['region_id'])
    this.props.setRegion(urlVars['region_id'], urlVars['name'])
    this.props.loadNews(urlVars['region_id'])
  }

  render() {
    const tags = this.props.tags.tags.map(item => ({
      value: (
        <Popover content={<div>Новостей: {item.count}</div>}>
          <span style={{ cursor: 'pointer' }}>{item.value}</span>
        </Popover>
      ),
      count: item.count,
      text_value: item.value,
    }))
    const antIcon = <Icon type="loading" style={{ fontSize: 40 }} spin />
    return (
      <>
        <TagCloud
          minSize={12}
          maxSize={35}
          tags={tags}
          onClick={tag => {
            console.log(tag)
            this.props.loadNews(this.props.tags.region_id, tag.text_value)
          }}
        />
        {tags.isLoadingTags && (
          <div className="TagsCloud__loader">
            <Spin indicator={antIcon} />
          </div>
        )}
      </>
    )
  }
}

const mapStateToProps = store => {
  return {
    tags: store.tags,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getTags: regionId => dispatch(getTags(regionId)),
    setRegion: (id, name) => dispatch(setRegion(id, name)),
    loadNews: (regionId, theme = null) => dispatch(loadNews(regionId, theme)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagsCloud)
