import React, { PureComponent } from 'react'
import { Row, Col } from 'antd'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'

import NewsList from '../../NewsList/NewsListComponent'
import TagsCloud from './Components/TagsCloudComponent'

class Tags extends PureComponent {
  render() {
    const region_name = this.props.tags.region_name
    return (
      <div>
        <Helmet>
          <title>{region_name}</title>
        </Helmet>

        <h1>{region_name}</h1>

        <Row>
          <Col span={9}>
            <TagsCloud />
          </Col>
          <Col span={15}>
            <NewsList
              data={this.props.tags.news.map((item, index) => ({
                id: item.id,
                animation: false,
                title: item.article_title,
                source: item.source_title,
                logo: '',
                sourceLink: item.url,
                newsLink: item.article_url,
                snipet: item.article_text,
              }))}
              isLoading={this.props.tags.isLoadingNews}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = store => {
  return {
    tags: store.tags,
  }
}

export default connect(mapStateToProps)(Tags)
