import React, { PureComponent } from 'react'
import { Table, Divider, Popconfirm, Spin, Icon, Button } from 'antd'
import { connect } from 'react-redux'

import SearchGroupForm from './SearchGroupFormComponent'

import {
  setActiveEditGroup,
  clearActiveEditGroup,
  deleteGroup,
} from '../../../../redux/actions/SearchFormActions'

const VIEW_MODE = {
  GROUPS_TABLE: 'GROUPS_TABLE',
  EDIT_GROUP: 'EDIT_GROUP',
}

class SearchGroups extends PureComponent {
  state = {
    viewMode: VIEW_MODE.GROUPS_TABLE,
  }

  confirmDelete = id => {
    this.props.deleteGroup(id)
  }

  enableEditMode = (e, record) => {
    e.preventDefault()
    this.setState({
      viewMode: VIEW_MODE.EDIT_GROUP,
    })
    this.props.setActiveEditGroup({
      id: record.id,
      name: record.name,
      regions: record.regions,
      search_requests: record.search_requests,
    })
  }

  enableCreateMode = e => {
    e.preventDefault()
    this.setState({
      viewMode: VIEW_MODE.EDIT_GROUP,
    })
  }

  disableEditMode = e => {
    e.preventDefault()
    this.props.clearActiveEditGroup()
    this.setState({
      viewMode: VIEW_MODE.GROUPS_TABLE,
    })
  }

  render() {
    const antIcon = <Icon type="loading" style={{ fontSize: 40 }} spin />

    const columns = [
      {
        title: 'Название',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Запросов',
        dataIndex: 'queries',
        key: 'queries',
      },
      {
        title: 'Действие',
        key: 'action',
        render: (text, record) => (
          <span>
            <a href="/" onClick={e => this.enableEditMode(e, record)}>
              Редактировать
            </a>
            <Divider type="vertical" />
            <Popconfirm
              title="Вы уверены, что хотите удалить группу запросов?"
              onConfirm={e => this.confirmDelete(record.id)}
              okText="Да"
              cancelText="Нет"
            >
              <a href="/">Удалить</a>
            </Popconfirm>
          </span>
        ),
      },
    ]

    const data = this.props.searchForm.search_groups.map(item => ({
      key: item.id,
      id: item.id,
      name: item.name,
      queries: item.search_requests.length,
      regions: item.regions,
      search_requests: item.search_requests,
    }))

    let returnDOM = null

    if (this.state.viewMode === VIEW_MODE.GROUPS_TABLE) {
      returnDOM = (
        <Spin
          spinning={this.props.searchForm.isLoadingSearchGroups}
          indicator={antIcon}
        >
          <Button type="primary" onClick={this.enableCreateMode}>
            Создать
          </Button>
          <Table columns={columns} dataSource={data} pagination={false} />
        </Spin>
      )
    } else if (this.state.viewMode === VIEW_MODE.EDIT_GROUP) {
      returnDOM = <SearchGroupForm disableEditMode={this.disableEditMode} />
    }

    return returnDOM
  }
}

const mapStateToProps = store => {
  return {
    searchForm: store.searchForm,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setActiveEditGroup: record => dispatch(setActiveEditGroup(record)),
    clearActiveEditGroup: () => dispatch(clearActiveEditGroup()),
    deleteGroup: id => dispatch(deleteGroup(id)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchGroups)
