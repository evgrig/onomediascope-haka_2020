import React, { PureComponent } from 'react'
import {
  Table,
  Popconfirm,
  message,
  Form,
  Input,
  Button,
  Select,
  Spin,
  Icon,
} from 'antd'
import uuid from 'uuid'
import { connect } from 'react-redux'

import {
  addSearchItem,
  removeSearchItem,
  updateGroupRegions,
  updateGroupName,
  updateGroup,
  createGroup,
} from '../../../../redux/actions/SearchFormActions'

const { Option } = Select
const InputGroup = Input.Group

class SearchGroupForm extends PureComponent {
  state = {
    request_name: '',
    request_regions: [],
  }

  handleSubmit = e => {
    e.preventDefault()
    const { searchForm } = this.props
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (searchForm.activeEditGroup.id) {
          this.props.updateGroup(searchForm.activeEditGroup)
        } else {
          this.props.createGroup(searchForm.activeEditGroup)
        }
      }
    })
  }

  confirmDeleteQuery = id => {
    this.props.removeSearchItem(id)
    message.success('Запрос удален')
  }

  updateRequestName = e => {
    this.setState({
      request_name: e.target.value,
    })
  }

  updateRequestRegions = value => {
    this.setState({
      request_regions: value,
    })
  }

  addRequest = () => {
    if (this.state.request_name === '') {
      message.error('Укажите текст запроса')
      return
    }
    this.props.addSearchItem({
      name: this.state.request_name,
      regions: this.state.request_regions,
      id: uuid(),
    })
    this.setState({
      request_name: '',
    })
  }

  render() {
    const antIcon = <Icon type="loading" style={{ fontSize: 40 }} spin />
    const { getFieldDecorator } = this.props.form
    const { searchForm } = this.props

    const columnsQueries = [
      {
        title: 'Название',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Регион',
        dataIndex: 'regions_names',
        key: 'regions_names',
      },
      {
        title: '',
        key: 'action',
        render: (text, record) => (
          <span>
            <Popconfirm
              title="Вы уверены, что хотите удалить запрос?"
              onConfirm={e => this.confirmDeleteQuery(record.id)}
              okText="Да"
              cancelText="Нет"
            >
              <a href="/">Удалить</a>
            </Popconfirm>
          </span>
        ),
      },
    ]

    const dataQueries = searchForm.activeEditGroup.search_requests.map(
      item => ({
        key: item.id,
        id: item.id,
        name: item.name,
        regions: item.regions,
        regions_names: item.regions
          .map((regId, i) => {
            const regions = searchForm.regions.filter(regItem => {
              return regItem.id === regId
            })
            if (regions.length > 0) {
              return regions[0].name
            } else {
              return null
            }
          })
          .join(', '),
      })
    )

    return (
      <>
        <p>
          <a href="/" onClick={this.props.disableEditMode}>
            Назад
          </a>
        </p>
        <Spin
          spinning={searchForm.isSearchGroupFormLoading}
          indicator={antIcon}
        >
          <Form
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 14 }}
            onSubmit={this.handleSubmit}
          >
            <Form.Item label="Название группы">
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Введите название группы' }],
                initialValue: searchForm.activeEditGroup.name,
              })(
                <Input
                  onChange={e => this.props.updateGroupName(e.target.value)}
                />
              )}
            </Form.Item>
            <Form.Item label="Регион поиска">
              {getFieldDecorator('region', {
                rules: [{ required: false }],
                initialValue: searchForm.activeEditGroup.regions,
              })(
                <Select
                  placeholder="Выберите"
                  onChange={value => this.props.updateGroupRegions(value)}
                  mode="multiple"
                >
                  {searchForm.regions.map(item => (
                    <Option value={item.id} key={item.id}>
                      {item.name}
                    </Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label="Поисковые запросы">
              <Table
                columns={columnsQueries}
                dataSource={dataQueries}
                pagination={false}
                className={'SearchGroups__queries-table'}
              />

              <InputGroup compact>
                <Input
                  placeholder="Поисковый запрос"
                  value={this.state.request_name}
                  onChange={this.updateRequestName}
                />
              </InputGroup>
              <Select
                placeholder="Регион поиска"
                mode="multiple"
                onChange={value => this.updateRequestRegions(value)}
              >
                {searchForm.regions.map(item => (
                  <Option value={item.id} key={item.id}>
                    {item.name}
                  </Option>
                ))}
              </Select>
              <Button onClick={this.addRequest}>
                Добавить запрос в группу
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ span: 12, offset: 5 }}>
              <Button type="primary" htmlType="submit">
                Сохранить
              </Button>
            </Form.Item>
          </Form>
        </Spin>
      </>
    )
  }
}

const mapStateToProps = store => {
  return {
    searchForm: store.searchForm,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addSearchItem: record => dispatch(addSearchItem(record)),
    removeSearchItem: id => dispatch(removeSearchItem(id)),
    updateGroupRegions: value => dispatch(updateGroupRegions(value)),
    updateGroupName: value => dispatch(updateGroupName(value)),
    updateGroup: group => dispatch(updateGroup(group)),
    createGroup: group => dispatch(createGroup(group)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create({ name: 'coordinated' })(SearchGroupForm))
