import React, { PureComponent } from 'react'
import { TreeSelect, Row, Col, Button, Checkbox, Switch, Drawer } from 'antd'
import { connect } from 'react-redux'

import SearchGroups from './SearchGroupsComponent'

import {
  getSearchGroups,
  getRegions,
} from '../../../../redux/actions/SearchFormActions'

const { SHOW_PARENT } = TreeSelect

class SearchForm extends PureComponent {
  state = {
    value: [],
    isOpenSearchGroupsForm: false,
    settings: {
      autoupdate: true,
      snipet: true,
      social: true,
      telegram: true,
      media: true,
    },
  }

  clearForm = () => {
    this.setState({
      value: [],
      settings: {
        autoupdate: true,
        snipet: true,
        social: true,
        telegram: true,
        media: true,
      },
    })
  }

  componentDidMount() {
    this.props.getSearchGroups()
    this.props.getRegions()
  }

  onChange = value => {
    console.log('onChange ', value)
    this.setState({ value })
  }

  onOpenSearchGroupsForm = () => {
    this.setState({ isOpenSearchGroupsForm: true })
  }

  onCloseSearchGroupsForm = () => {
    this.setState({ isOpenSearchGroupsForm: false })
  }

  updateSettings = (param, value) => {
    this.setState({
      settings: {
        ...this.state.settings,
        [param]: value,
      },
    })
  }

  render() {
    const treeData = this.props.searchForm.search_groups.map(item => {
      return {
        title: item.name,
        value: item.id,
        key: item.id,
        children: item.search_requests.map(request => ({
          title: request.name,
          value: request.id,
          key: request.id,
        })),
      }
    })

    const tProps = {
      treeData,
      value: this.state.value,
      onChange: this.onChange,
      treeCheckable: true,
      showCheckedStrategy: SHOW_PARENT,
      searchPlaceholder: 'Выберите запрос или группу запросов',
      style: {
        width: '100%',
      },
    }

    const { settings } = this.state
    return (
      <>
        <Row gutter={16}>
          <Col span={22}>
            <TreeSelect {...tProps} />

            <Row className={'SearchForm__container'}>
              <Col span={4}>
                <Switch
                  checked={settings.autoupdate}
                  onChange={value => this.updateSettings('autoupdate', value)}
                />
              </Col>
              <Col span={20}>Автоматическое обновление результатов</Col>
            </Row>

            <Row className={'SearchForm__container'}>
              <Col span={4}>
                <Switch
                  checked={settings.snipet}
                  onChange={value => this.updateSettings('snipet', value)}
                />
              </Col>
              <Col span={20}>Показывать снипет</Col>
            </Row>

            <Row className={'SearchForm__container'}>
              <Col span={6}>
                <Checkbox
                  checked={settings.social}
                  onChange={e => {
                    this.updateSettings('social', e.target.checked)
                  }}
                >
                  Cоцсети
                </Checkbox>
              </Col>
              <Col span={10}>
                <Checkbox
                  checked={settings.telegram}
                  onChange={e => {
                    this.updateSettings('telegram', e.target.checked)
                  }}
                >
                  Телеграм-каналы
                </Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox
                  checked={settings.media}
                  onChange={e => {
                    this.updateSettings('media', e.target.checked)
                  }}
                >
                  Онлайн СМИ
                </Checkbox>
              </Col>
            </Row>

            {/*
            <Button
              type="danger"
              block
              className={'SearchForm__container'}
              onClick={this.clearForm}
            >
              Очистить форму
            </Button>
            */}
            <Button
              block
              type="primary"
              className={'SearchForm__container'}
              style={{ marginBottom: '2rem' }}
            >
              Найти
            </Button>

            <Button block onClick={this.onOpenSearchGroupsForm}>
              Настроить группы
            </Button>
          </Col>
        </Row>
        <Drawer
          title="Группы поиска"
          width={600}
          onClose={this.onCloseSearchGroupsForm}
          visible={this.state.isOpenSearchGroupsForm}
        >
          <SearchGroups />
        </Drawer>
      </>
    )
  }
}

const mapStateToProps = store => {
  return {
    searchForm: store.searchForm,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getSearchGroups: () => dispatch(getSearchGroups()),
    getRegions: () => dispatch(getRegions()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchForm)
