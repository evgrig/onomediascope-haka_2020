import React from 'react'
import { Row, Col } from 'antd'
import { Helmet } from 'react-helmet'

import NewsList from '../../NewsList/NewsListComponent'
import SearchForm from './Components/SearchFormComponent'

const Search = props => (
  <div>
    <Helmet>
      <title>Поиск</title>
    </Helmet>

    <h1>Поиск</h1>

    <Row>
      <Col span={9}>
        <SearchForm />
      </Col>
      <Col span={15}>
        <NewsList animation={false} drillDown={true} />
      </Col>
    </Row>
  </div>
)

export default Search
