import React, { PureComponent } from 'react'
import { Router, Switch, Route, Link, Redirect } from 'react-router-dom'
import { history } from '../../redux/configureStore'
import { Layout, Menu, message } from 'antd'

import NotFound from '../Pages/NotFound/NotFoundComponent'
import Search from '../Pages/Search/SearchComponent'
import Timeline from '../Pages/Timeline/TimelineComponent'
import Tags from '../Pages/Tags/TagsComponent'

import { connect } from 'react-redux'
import { logOutUser } from '../../redux/actions/UserActions'

class MainLayout extends PureComponent {
  state = {
    collapsed: false,
  }

  onCollapse = collapsed => {
    this.setState({ collapsed })
  }

  logOut(e) {
    e.preventDefault()
    this.props.logOutUser()
  }

  componentDidMount() {
    this.checkAuth()
  }

  componentDidUpdate() {
    if (this.props.server.error.status) {
      message.error(
        this.props.server.error.status + ': ' + this.props.server.error.text
      )
    }

    this.checkAuth()
  }

  checkAuth() {
    if (!this.props.user.authorized) {
      document.location = '/login'
    }
  }

  render() {
    const { user } = this.props
    const pathname = document.location.pathname

    if (!user.authorized) {
      return null
    }

    const { Header, Content, Footer } = Layout
    const date = new Date()

    let selectKey = '1'
    switch (pathname) {
      case '/login':
        selectKey = '1'
        break
      case '/':
        selectKey = '1'
        break
      case '/timeline':
        selectKey = '2'
        break
      default:
        selectKey = '0'
        break
    }

    return (
      <Layout>
        <Header className="Header">
          <div className="Header__userInfo">
            {user.attributes.name}
            <Link
              className="Header__exit-link"
              onClick={this.logOut.bind(this)}
              to=""
            >
              Выход
            </Link>
          </div>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[selectKey]}
            style={{ lineHeight: '64px' }}
          >
            <Menu.Item key="1">
              <Link to="/">Поиск</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/timeline">Лента</Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Content className="Content">
          <div className="Content__container">
            <Router history={history}>
              <Switch>
                <Redirect from="/login" to="/" />

                <Route exact path="/" component={Search} />
                <Route exact path="/timeline" component={Timeline} />
                <Route exact path="/tags" component={Tags} />

                <Route component={NotFound} />
              </Switch>
            </Router>
          </div>
        </Content>
        <Footer className="Footer">
          ©{date.getFullYear()},{' '}
          <a
            href="https://onomedia.today"
            rel="noopener noreferrer"
            target="_blank"
          >
            ONO Media
          </a>
        </Footer>
      </Layout>
    )
  }
}

const mapStateToProps = store => {
  return {
    user: store.user,
    server: store.server,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logOutUser: () => dispatch(logOutUser()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainLayout)
