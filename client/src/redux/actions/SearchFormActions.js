export const SEARCH_GROUPS_REQUEST = 'SEARCH_GROUPS_REQUEST'
export const SEARCH_GROUPS_SUCCESS = 'SEARCH_GROUPS_SUCCESS'

export function getSearchGroups() {
  return dispatch => {
    dispatch({
      types: [SEARCH_GROUPS_REQUEST, SEARCH_GROUPS_SUCCESS],
      payload: {
        request: {
          method: 'GET',
          url: '/monitor/get-search-groups',
        },
      },
    })
  }
}

export const REGIONS_REQUEST = 'REGIONS_REQUEST'
export const REGIONS_SUCCESS = 'REGIONS_SUCCESS'

export function getRegions() {
  return dispatch => {
    dispatch({
      types: [REGIONS_REQUEST, REGIONS_SUCCESS],
      payload: {
        request: {
          method: 'GET',
          url: '/outputs/get-regions',
        },
      },
    })
  }
}

export const SET_ACTIVE_EDIT_GROUP = 'SET_ACTIVE_EDIT_GROUP'

export function setActiveEditGroup(record) {
  return dispatch => {
    dispatch({
      type: SET_ACTIVE_EDIT_GROUP,
      payload: record,
    })
  }
}

export const CLEAR_ACTIVE_EDIT_GROUP = 'CLEAR_ACTIVE_EDIT_GROUP'

export function clearActiveEditGroup(record) {
  return dispatch => {
    dispatch({
      type: CLEAR_ACTIVE_EDIT_GROUP,
    })
  }
}

export const ADD_SEARCH_ITEM = 'ADD_SEARCH_REQUEST'

export function addSearchItem(record) {
  return dispatch => {
    dispatch({
      type: ADD_SEARCH_ITEM,
      payload: {
        record,
      },
    })
  }
}

export const REMOVE_SEARCH_ITEM = 'REMOVE_SEARCH_ITEM'

export function removeSearchItem(id) {
  return dispatch => {
    dispatch({
      type: REMOVE_SEARCH_ITEM,
      payload: {
        id,
      },
    })
  }
}

export const UPDATE_GROUP_REGIONS = 'UPDATE_GROUP_REGIONS'

export function updateGroupRegions(value) {
  return dispatch => {
    dispatch({
      type: UPDATE_GROUP_REGIONS,
      payload: {
        regions: value,
      },
    })
  }
}

export const UPDATE_GROUP_NAME = 'UPDATE_GROUP_NAME'

export function updateGroupName(value) {
  return dispatch => {
    dispatch({
      type: UPDATE_GROUP_NAME,
      payload: {
        name: value,
      },
    })
  }
}

export const UPDATE_GROUP_REQUEST = 'UPDATE_GROUP_REQUEST'
export const UPDATE_GROUP_SUCCESS = 'UPDATE_GROUP_SUCCESS'

export function updateGroup(group) {
  return dispatch => {
    Promise.all([
      dispatch({
        types: [UPDATE_GROUP_REQUEST, UPDATE_GROUP_SUCCESS],
        payload: {
          request: {
            method: 'PATCH',
            url: '/monitor/update-search-group',
            data: group,
          },
        },
      }),
    ]).then(() => {
      dispatch({
        types: [SEARCH_GROUPS_REQUEST, SEARCH_GROUPS_SUCCESS],
        payload: {
          request: {
            method: 'GET',
            url: '/monitor/get-search-groups',
          },
        },
      })
    })
  }
}

export const CREATE_GROUP_REQUEST = 'CREATE_GROUP_REQUEST'
export const CREATE_GROUP_SUCCESS = 'CREATE_GROUP_SUCCESS'

export function createGroup(group) {
  return dispatch => {
    Promise.all([
      dispatch({
        types: [CREATE_GROUP_REQUEST, CREATE_GROUP_SUCCESS],
        payload: {
          request: {
            method: 'POST',
            url: '/monitor/create-search-group',
            data: group,
          },
        },
      }),
    ]).then(() => {
      dispatch({
        types: [SEARCH_GROUPS_REQUEST, SEARCH_GROUPS_SUCCESS],
        payload: {
          request: {
            method: 'GET',
            url: '/monitor/get-search-groups',
          },
        },
      })
    })
  }
}

export const DELETE_GROUP_REQUEST = 'DELETE_GROUP_REQUEST'
export const DELETE_GROUP_SUCCESS = 'DELETE_GROUP_SUCCESS'

export function deleteGroup(id) {
  return dispatch => {
    dispatch({
      types: [DELETE_GROUP_REQUEST, DELETE_GROUP_SUCCESS],
      payload: {
        request: {
          method: 'DELETE',
          url: '/monitor/delete-search-group',
          data: {
            id,
          },
        },
      },
    })
  }
}
