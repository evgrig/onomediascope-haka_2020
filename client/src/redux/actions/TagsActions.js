export const TAGS_REQUEST = 'TAGS_REQUEST'
export const TAGS_SUCCESS = 'TAGS_SUCCESS'

export function getTags(regionId) {
  return dispatch => {
    dispatch({
      types: [TAGS_REQUEST, TAGS_SUCCESS],
      payload: {
        request: {
          method: 'GET',
          url: '/hack/tags/' + regionId,
        },
      },
    })
  }
}

export const SET_REGION = 'SET_REGION'

export function setRegion(id, name) {
  return dispatch => {
    dispatch({
      type: SET_REGION,
      payload: {
        id,
        name,
      },
    })
  }
}

export const TAGS_LOAD_NEWS_REQUEST = 'TAGS_LOAD_NEWS_REQUEST'
export const TAGS_LOAD_NEWS_SUCCESS = 'TAGS_LOAD_NEWS_SUCCESS'

export function loadNews(regionId, theme = null) {
  return dispatch => {
    dispatch({
      types: [TAGS_LOAD_NEWS_REQUEST, TAGS_LOAD_NEWS_SUCCESS],
      payload: {
        request: {
          method: 'GET',
          url: 'hack/vk/' + regionId + '/' + encodeURI(theme),
        },
      },
    })
  }
}
