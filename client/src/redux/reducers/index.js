import { combineReducers } from 'redux'
import user from './user'
import server from './server'
import searchForm from './searchForm'
import tags from './tags'

export default combineReducers({
  user,
  server,
  searchForm,
  tags,
})
