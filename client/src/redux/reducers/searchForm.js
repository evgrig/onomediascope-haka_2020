import produce from 'immer'
import {
  SEARCH_GROUPS_REQUEST,
  SEARCH_GROUPS_SUCCESS,
  REGIONS_REQUEST,
  REGIONS_SUCCESS,
  SET_ACTIVE_EDIT_GROUP,
  CLEAR_ACTIVE_EDIT_GROUP,
  ADD_SEARCH_ITEM,
  REMOVE_SEARCH_ITEM,
  UPDATE_GROUP_REGIONS,
  UPDATE_GROUP_NAME,
  UPDATE_GROUP_REQUEST,
  UPDATE_GROUP_SUCCESS,
  CREATE_GROUP_REQUEST,
  CREATE_GROUP_SUCCESS,
  DELETE_GROUP_REQUEST,
  DELETE_GROUP_SUCCESS,
} from '../actions/SearchFormActions'

const initialState = {
  isLoadingSearchGroups: false,
  search_groups: [],
  isLoadingRegions: false,
  regions: [],

  isSearchGroupFormLoading: false,
  activeEditGroup: {
    id: null,
    name: '',
    regions: [],
    search_requests: [],
  },
}

export default function(state = initialState, action) {
  switch (action.type) {
    case DELETE_GROUP_REQUEST:
      return produce(state, draftState => {
        draftState.isLoadingSearchGroups = true
      })
    case DELETE_GROUP_SUCCESS:
      return produce(state, draftState => {
        draftState.isLoadingSearchGroups = false
        draftState.search_groups = action.payload.data
      })
    case CREATE_GROUP_REQUEST:
      return produce(state, draftState => {
        draftState.isSearchGroupFormLoading = true
      })
    case CREATE_GROUP_SUCCESS:
      return produce(state, draftState => {
        draftState.isSearchGroupFormLoading = false
        draftState.activeEditGroup.id = action.payload.data.id
      })
    case UPDATE_GROUP_REQUEST:
      return produce(state, draftState => {
        draftState.isSearchGroupFormLoading = true
      })
    case UPDATE_GROUP_SUCCESS:
      return produce(state, draftState => {
        draftState.isSearchGroupFormLoading = false
      })
    case UPDATE_GROUP_NAME:
      return produce(state, draftState => {
        draftState.activeEditGroup.name = action.payload.name
      })
    case UPDATE_GROUP_REGIONS:
      return produce(state, draftState => {
        draftState.activeEditGroup.regions = action.payload.regions
      })
    case ADD_SEARCH_ITEM:
      return produce(state, draftState => {
        draftState.activeEditGroup.search_requests.push(action.payload.record)
      })
    case REMOVE_SEARCH_ITEM:
      return produce(state, draftState => {
        let position = -1
        draftState.activeEditGroup.search_requests.forEach((item, i) => {
          if (item.id === action.payload.id) {
            position = i
          }
        })
        if (position >= 0) {
          draftState.activeEditGroup.search_requests.splice(position, 1)
        }
      })
    case SET_ACTIVE_EDIT_GROUP:
      return produce(state, draftState => {
        draftState.activeEditGroup = action.payload
      })
    case CLEAR_ACTIVE_EDIT_GROUP:
      return produce(state, draftState => {
        draftState.activeEditGroup = initialState.activeEditGroup
      })
    case SEARCH_GROUPS_REQUEST:
      return produce(state, draftState => {
        draftState.isLoadingSearchGroups = true
      })
    case SEARCH_GROUPS_SUCCESS:
      return produce(state, draftState => {
        draftState.isLoadingSearchGroups = false
        draftState.search_groups = action.payload
      })
    case REGIONS_REQUEST:
      return produce(state, draftState => {
        draftState.isLoadingRegions = true
      })
    case REGIONS_SUCCESS:
      return produce(state, draftState => {
        draftState.isLoadingRegions = false
        draftState.regions = action.payload
      })

    default:
      return state
  }
}
