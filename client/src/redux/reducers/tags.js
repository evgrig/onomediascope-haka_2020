import produce from 'immer'
import {
  TAGS_REQUEST,
  TAGS_SUCCESS,
  SET_REGION,
  TAGS_LOAD_NEWS_REQUEST,
  TAGS_LOAD_NEWS_SUCCESS,
} from '../actions/TagsActions'

const initialState = {
  region_id: null,
  region_name: null,
  isLoadingTags: false,
  tags: [],

  news: [],
  isLoadingNews: false,
}

export default function(state = initialState, action) {
  switch (action.type) {
    case TAGS_LOAD_NEWS_REQUEST:
      return produce(state, draftState => {
        draftState.news = []
        draftState.isLoadingNews = true
      })
    case TAGS_LOAD_NEWS_SUCCESS:
      return produce(state, draftState => {
        draftState.isLoadingNews = false
        draftState.news = action.payload
      })
    case TAGS_REQUEST:
      return produce(state, draftState => {
        draftState.isLoadingTags = true
      })
    case TAGS_SUCCESS:
      return produce(state, draftState => {
        draftState.isLoadingTags = false
        draftState.tags = action.payload
      })
    case SET_REGION:
      return produce(state, draftState => {
        draftState.region_id = action.payload.id
        draftState.region_name = action.payload.name
      })
    default:
      return state
  }
}
