## Installation

```bash
yarn global add stylus concurrently nib
yarn
```

## Get started

```bash
yarn start
```

## Hint

To comfortable coding and passing pre-commit test install in your code editor ESLint and Prettier plugins. It was configured to fail the test if find one warning at least.

May the force come with you!
